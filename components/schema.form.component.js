module.exports = function (ngModule) {
    ngModule.component('schemaForm', {
        template: require('./schema.form.component.html'),
        controllerAs: 'SCHEMA',
        bindings: {
            schema: '<',
            model: '=',
            schemaInitialized: '&'
        },
        controller: ['$rootScope', 'SchemaToFormly', function ($rootScope, SchemaToFormly) {
            var SCHEMA = this
            var scope = $rootScope.$new()

            SCHEMA.initialize = function () {
                console.log('Got model input in Schema form ', angular.copy(SCHEMA.model))
                initializeFormly()

                scope.$watch('formlyModel', function (newValue, oldValue) {
                    if (typeof newValue != 'undefined') {
                        //console.log('newValue ', newValue)
                        SCHEMA.model = JSON.parse(angular.toJson(newValue))
                    }
                }, true)



                var handle = {
                    clear: function () {
                        Object.keys(SCHEMA.formlyModel)
                            .map(function (key) {
                                delete SCHEMA.formlyModel[key]
                            })
                    }
                }

                if (SCHEMA.schemaInitialized) {
                    SCHEMA.schemaInitialized({
                        handle: handle
                    })
                }
            }

            function initializeFormly() {
                SCHEMA.formlyFields = SchemaToFormly.toFormly(SCHEMA.schema, SCHEMA.model)
                if (SCHEMA.model) {
                    SCHEMA.formlyModel = SCHEMA.model
                } else {
                    SCHEMA.formlyModel = angular.extend({}, getFromMemory())
                }


                scope.formlyModel = SCHEMA.formlyModel
            }

            function getFromMemory() {

            }
        }]
    })
}
