module.exports = function (ngModule) {
    ngModule.constant('defaultFormlyJson', {
        string: {
            type: 'input',
            templateOptions: {
                type: 'text'
            }
        },
        number: {
            type: 'input',
            templateOptions: {
                type: 'number'
            }
        },
        boolean: {
            type: 'checkbox'
        },
        object: {
            type: 'select'
        },
        action: {
            type: 'action'
        }
    })
}
