module.exports = function(ngModule) {

    ngModule.run(function(formlyConfig) {
        formlyConfig.setType({
            name: 'action',
            template: require('./action.formly.html'),
            controller: ['$scope', 'SchemaActions',
                function($scope, SchemaActions) {

                    $scope.context = {
                        state: 'idle'
                    }
                    $scope.action = function(model, options) {
                        console.log('model - ', model)
                        console.log('options- ', options)
                        SchemaActions.action(model, $scope.context, options.action)
                    }

                    $scope.buttonLabel = function() {
                        var options = $scope.options
                        if (options.interface) {
                            if (options.interface.progressLabel && $scope.context.state == 'processing') {
                                return options.interface.progressLabel
                            } else {
                                return options.interface.label
                            }
                        } else {
                            return 'Click'
                        }
                    }

                }
            ]
        });
    })

}
