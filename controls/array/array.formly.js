module.exports = function (ngModule) {

    ngModule.run(function (formlyConfig) {
        formlyConfig.setType({
            name: 'array',
            template: require('./array.component.html'),
            wrapper: 'bootstrapLabel',
            controller: ['$scope', '$timeout',
                function ($scope, $timeout) {
                    var array_model = null, schemaHandle = null
                    $scope.itemSchema = {}
                    $scope.isField = false

                    $scope.createSingleField = function () {
                        var field_schema = angular.copy($scope.options.field_schema)
                        var form_schema = $scope.options.form_schema
                        var repeat_schema = form_schema
                        if (!form_schema) {
                            field_schema.key = 'value'
                            repeat_schema = {
                                "title": "Text Inputs",
                                "type": "object",
                                "properties": [field_schema]
                            }
                            $scope.isField = true
                        }

                        $scope.itemSchema.form = repeat_schema


                        if (!angular.isArray($scope.model[$scope.options.key])) {
                            $scope.model[$scope.options.key] = []
                        }
                        array_model = $scope.model[$scope.options.key]
                    }

                    $scope.addValue = function (item_value) {
                        console.log('Scope model ', $scope.model)
                        console.log('item_value ', item_value)
                        if ($scope.isField) {
                            //debugger
                            array_model.push(item_value.value)
                            //item_value.value = null
                            delete item_value.value
                            $timeout(function () { }, 0)
                            console.log('item_value ', item_value)
                        } else {
                            array_model.push(angular.copy(item_value))
                            $scope.itemSchema.model = {}
                        }
                        //debugger
                        if (schemaHandle) {
                            schemaHandle.clear()
                        }
                    }

                    $scope.removeValue = function (index) {
                        array_model.splice(index, 1)
                    }

                    $scope.itemSchemaInitialized = function (H) {
                        schemaHandle = H
                        //console.log('Item handle in array formly', H)
                    }
                }]
        })
    })

}
