module.exports = function(ngModule) {

    ngModule.run(function(formlyConfig) {
        formlyConfig.setType({
            name: 'checklist',
            template: require('./checklist.formly.html'),
            wrapper: 'bootstrapLabel',
            controller: ['$scope', function($scope) {

            }]
        });
    })

}
