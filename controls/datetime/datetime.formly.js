module.exports = function(ngModule) {

    ngModule.run(function(formlyConfig) {
        formlyConfig.setType({
            name: 'datetime',
            template: require('./datetime.component.html'),
            wrapper: 'bootstrapLabel',
            controller: ['$scope', function($scope) {

                var options = $scope.options

                $scope.values = {
                    displayFormat: "dd MMM yyyy HH:mm"
                }
                if (options && options.interface && !options.interface.showTime) {
                    $scope.values.displayFormat = "dd MMM yyyy"
                }

                if (options && options.interface && !options.interface.showDate) {
                    $scope.values.displayFormat = "HH:mm"
                }
                $scope.changed = function(datetime) {
                    console.log('ng changed called')
                    var key = $scope.options.key
                    var model = $scope.model
                    var format = $scope.options.format
                    console.log($scope.options)
                    console.log('changed datetime ', datetime)
                    console.log('key- ', $scope.options.key, ' format ', format)

                    model[key] = angular.moment(datetime).format(format)
                }
            }]
        });
    })

}
