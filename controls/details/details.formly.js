module.exports = function (ngModule) {

    ngModule.run(function (formlyConfig) {
        formlyConfig.setType({
            name: 'details',
            template: require('./details.component.html'),
            wrapper: 'bootstrapLabel',
            controller: ['$scope', function ($scope) {
                $scope.details = {}
                var listFormly = null
                $scope.initElementSchema = function () {
                    //associate model output to details
                    $scope.model[$scope.options.key] = $scope.details

                    //make a list schema to show the dropdown list
                    //copy the same schema and change type to select                    
                    listFormly = angular.copy($scope.options)
                    listFormly.type = "select"
                    listFormly.templateOptions.label = "Select"
                    listFormly.key = "selection"
                    $scope.listFormly = [listFormly]
                    $scope.listSelection = {}

                    //watch list selection and remove any elements from details which have been un selected
                    $scope.$watch('listSelection.selection', function (selection, oldVal) {
                        if (selection && selection.length && angular.isString(selection[0])) {
                            //console.log('new value of selection ', $scope.listSelection.selection)
                            //check all keys in details. if a key is not present in list selection, delete the key
                            Object.keys($scope.details).map(function (key) {
                                if (selection.indexOf(key) < 0) {
                                    delete $scope.details[key]
                                    console.log('Key ', key, ' is not present in selection ', selection)
                                }
                            })
                        }
                    }, true)
                }

                $scope.selectionLabel = function (item) {
                    if (listFormly) {
                        var matching = listFormly.templateOptions.options.filter(function (option) {
                            return option.value == item
                        })
                        if (matching && matching.length == 1) {
                            return matching[0].label
                        } else {
                            return ''
                        }

                    }
                }
            }]
        });
    })


}
