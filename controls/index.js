module.exports = function (ngModule) {

    require('./toggle/toggle.formly')(ngModule)
    require('./datetime/datetime.formly')(ngModule)

    require('./slider/slider.formly')(ngModule)
    require('./checklist/checklist.formly')(ngModule)
    require('./action/action.formly')(ngModule)

    require('./select/select.formly')(ngModule)
    require('./details/details.formly')(ngModule)

    require('./picture/picture.formly')(ngModule)

    require('./text/text.formly')(ngModule)

    require('./array/array.formly')(ngModule)
    require('./webPicture/webPicture.formly')(ngModule)
}
