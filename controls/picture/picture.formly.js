module.exports = function (ngModule) {

    ngModule.run(function (formlyConfig) {
        formlyConfig.setType({
            name: 'picture',
            template: require('./picture.component.html'),
            wrapper: 'bootstrapLabel',
            controller: ['$scope', 'Camera', '$q', 'cloudinary', function ($scope, Camera, $q, cloudinary) {

                $scope.getPicture = function (sourceType) {

                    var picture = $scope.options.interface.picture
                    var target = picture.targetWidth
                    var options = {
                        sourceType: sourceType,
                        quality: 100,
                        // targetWidth: picture.targetWidth,
                        correctOrientation: picture.correctOrientation
                    };
                    var image_url = null
                    var targetWidth = picture.targetWidth
                    //alert('before camera.get picture')
                    Camera.getPicture(options)
                        .then(function (imageUrl) {
                            //do image scaling and then convert to base64
                            image_url = imageUrl
                            $scope.image_1 = imageUrl
                            return computeHeight(imageUrl)
                        })
                        .then(function (targetHeight) {
                            //alert('targetHeight resolved with '+targetHeight) 
                            // return scaleImage(image_url, targetWidth, targetHeight)
                            return scaleImageCordova1(image_url, targetWidth, targetHeight)
                        })
                        .then(function (scaledImage) {
                            //alert('scaledImage resolved with '+JSON.stringify(scaledImage))
                            $scope.image_2 = scaledImage
                            return convertToBase64(scaledImage)
                            // return $q.resolve(scaledBase64Image)
                        }).then(function (base64Data) {

                            $scope.image_3 = base64Data

                            return uploadToCloudinary(base64Data)
                        }).then(function (url) {

                            $scope.image_4 = url
                            ////alert('url is '+JSON.stringify(url))
                            $scope.picture = url;
                            $scope.model[$scope.options.key] = url
                        }).catch(function (err) {
                            console.log(err);
                            //alert('err is '+JSON.stringify(err))
                        });

                };


                function computeHeight(image_url) {
                    //alert('in computeHeight with ',image_url)
                    var D = $q.defer();
                    // calculate appropriate height for width = 1070
                    //create an img element and use it to 

                    var img = new Image();
                    img.onload = function () {
                        var width = img.naturalWidth,
                            height = img.naturalHeight,
                            targetHeight = Math.round((1070 / width) * height)
                        console.log('height values - ', targetHeight, width, height)
                        //alert('updated values - '+targetHeight+' original-' +width+' '+height)
                        D.resolve(targetHeight)

                    };
                    img.src = image_url;
                    return D.promise;
                }

                function scaleImageCordova1(url, width, height) {
                    var D = $q.defer()
                    var options = {
                        uri: url,
                        folderName: "urbanflick",
                        quality: 90,
                        width: width,
                        height: height
                    };

                    window.ImageResizer.resize(options,
                        function (image) {
                            //alert('resolveing with scaled image '+JSON.stringify(image))
                            D.resolve(image)
                            // success: image is the new resized image
                        }, function (err) {
                            //alert('err occured in ImageResizer '+JSON.stringify(err))
                            D.reject(err)
                            // failed: grumpy cat likes this function
                        });
                    return D.promise
                }


                function convertToBase64(url) {
                    var D = $q.defer();

                    var xhr = new XMLHttpRequest();
                    xhr.onload = function () {
                        var reader = new FileReader();
                        reader.onloadend = function () {
                            // callback(reader.result);
                            D.resolve(reader.result)
                        }
                        reader.readAsDataURL(xhr.response);
                    };
                    xhr.open('GET', url);
                    xhr.responseType = 'blob';
                    xhr.send();

                    return D.promise;
                }

                function uploadToCloudinary(base64Img) {
                    var D = $q.defer();

                    cloudinary.upload(base64Img, {})
                        // This returns a promise that can be used for result handling
                        .then(function (resp) {
                            console.log('resp is ', resp.data.secure_url)
                            ////alert('image uploaded'+JSON.stringify(resp.data.secure_url))
                            var new_image_object = {
                                url: resp.data.secure_url,
                                data: resp.data
                            }
                            D.resolve(resp.data.secure_url)
                        })
                        .catch(function (err) {
                            console.log('err occured ' + JSON.stringify(err))
                            //alert('err in cloudinary.upload '+JSON.stringify(err))
                            D.reject(err)
                        })
                    return D.promise;
                }


            }]
        });
    })

}
