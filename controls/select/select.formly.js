module.exports = function (ngModule) {

    ngModule.run(function (formlyConfig) {
        formlyConfig.setType({
            name: 'select',
            template: require('./select.component.html'),
            wrapper: 'bootstrapLabel',
            controller: ['$scope', function ($scope) {
                $scope.multi = []

                //For array datatype
                if ($scope.options && $scope.options.dataType == 'array') {
                    var track_key = "model." + $scope.options.key
                    //watch the multi select model, extract values from label, value array
                    $scope.$watch(track_key, function (newVal, oldVal) {
                        console.log('some change in selection')
                        console.log('newVal', newVal)

                        if (newVal) {
                            $scope.model[$scope.options.key] = newVal.map(function (selection) {
                                if (selection.label && selection.value) {
                                    return selection.value
                                } else {
                                    return selection
                                }

                            })
                        }

                    }, true)
                }

            }]
        });
    })

}
