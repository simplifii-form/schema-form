module.exports = function(ngModule) {

    ngModule.run(function(formlyConfig) {
        formlyConfig.setType({
            name: 'slider',
            template: '<div><rzslider rz-slider-model="model[options.key]" rz-slider-options="sliderOptions"></rzslider></div>',
            wrapper: 'bootstrapLabel',
            controller: ['$scope', function($scope) {
                var options = $scope.options

                var sliderOptions = {
                    "floor": floor(),
                    "ceil": ceil()
                }

                $scope.sliderOptions = sliderOptions

                function maxValue() {
                    if (options.validations && angular.isDefined(options.validations.maxValue)) {
                        return options.validations.maxValue
                    }
                }

                function minValue() {
                    if (options.validations && angular.isDefined(options.validations.minValue)) {
                        return options.validations.minValue
                    }
                }

                function floor() {
                    if (options.interface && angular.isDefined(options.interface.floor)) {
                        return options.interface.floor
                    } else {
                        return minValue()
                    }
                }

                function ceil() {
                    if (options.interface && angular.isDefined(options.interface.ceil)) {
                        return options.interface.ceil
                    } else {
                        return maxValue()
                    }
                }


            }]
        });
    })

    require('./rzslider.min.css')
}
