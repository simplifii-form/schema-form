module.exports = function (ngModule) {

    ngModule.run(function (formlyConfig) {
        formlyConfig.setType({
            name: 'text',
            template: require('./text.component.html'),
            wrapper: 'bootstrapLabel'
        });
    })

}
