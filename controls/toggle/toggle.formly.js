module.exports = function(ngModule) {

    ngModule.run(function(formlyConfig) {
        formlyConfig.setType({
            name: 'toggle',
            template: '<div><switch ng-model="model[options.key]" on="{{options.interface.onLabel}}" off="{{options.interface.offLabel}}"></switch></div>',
            wrapper: 'bootstrapLabel'
        });
    })

    require('./angular-ui-switch.min.css')

}
