module.exports = function (ngModule) {

    ngModule.run(function (formlyConfig) {
        formlyConfig.setType({
            name: 'webpicture',
            template: require('./webPicture.component.html'),
            wrapper: 'bootstrapLabel',
            controller: ['$scope', 'Camera', '$q', 'cloudinary', 'alertService',
                function ($scope, Camera, $q, cloudinary, alertService) {
                    var targetWidth = 0
                    $scope.initial = true

                    $scope.selectFile = function () {
                        $("#urbanflick-web-image").click();
                    }
                    $scope.fileNameChaged = function () {
                        alert("select file");
                    }
                    // Takes a data URI and returns the Data URI corresponding to the resized image at the wanted size.
                    function resizedataURL(datas, wantedWidth, wantedHeight) {
                        var D = $q.defer()
                        // We create an image to receive the Data URI
                        var img = document.createElement('img');

                        // When the event "onload" is triggered we can resize the image.
                        img.onload = function () {
                            // We create a canvas and get its context.
                            var canvas = document.createElement('canvas');
                            var ctx = canvas.getContext('2d');

                            // We set the dimensions at the wanted size.
                            canvas.width = wantedWidth;
                            canvas.height = wantedHeight;

                            // We resize the image with the canvas method drawImage();
                            ctx.drawImage(this, 0, 0, wantedWidth, wantedHeight);

                            var dataURI = canvas.toDataURL();

                            D.resolve(dataURI)
                            /////////////////////////////////////////
                            // Use and treat your Data URI here !! //
                            /////////////////////////////////////////
                        };

                        // We put the Data URI in the image's src attribute
                        img.src = datas;
                        return D.promise
                    }
                    function readImageFromElement(input) {
                        var D = $q.defer()
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();

                            reader.onload = function (e) {
                                // console.log('e.target.result is ',e.target.result,e.target)
                                D.resolve(e.target.result)
                            };

                            reader.readAsDataURL(input.files[0]);
                        }
                        return D.promise
                    }
                    $scope.getPicture = function (sourceType) {
                        $scope.wait = true
                        $scope.initial = false
                        var picture = $scope.options.interface.webpicture
                        console.log('picture is ', picture)
                        var target = picture.targetWidth
                        var options = {
                            sourceType: sourceType,
                            quality: 100,
                            // targetWidth: picture.targetWidth,
                            correctOrientation: picture.correctOrientation
                        };
                        var image_data = null
                        targetWidth = picture.targetWidth
                        //alert('before camera.get picture')
                        var file_element = document.getElementById("urbanflick-web-image");
                        image_to_upload = file_element.files[0]

                        // Camera.getPicture(options)
                        // .then(function (imageUrl) { 
                        //do image scaling and then convert to base64
                        $scope.current_status = 'reading'
                        readImageFromElement(file_element)
                            .then(function (imageData) {
                                image_data = imageData
                                return computeHeight(imageData)
                            })
                            .then(function (computedHeight) {
                                $scope.current_status = 'resizing'
                                return resizedataURL(image_data, targetWidth, computedHeight)

                            })
                            .then(function (resized_image) {
                                $scope.image_3 = resized_image

                                $scope.current_status = 'uploading'
                                return uploadToCloudinary(resized_image)
                            }).then(function (url) {

                                alertService.showAlert('Image uploaded, scroll down to preview')
                                $scope.wait = false
                                $scope.initial = true
                                $scope.image_4 = url
                                ////alert('url is '+JSON.stringify(url))
                                $scope.picture = url;
                                $scope.model[$scope.options.key] = url
                            }).catch(function (err) {
                                console.log(err);
                                //alert('err is '+JSON.stringify(err))
                            });

                    };


                    function computeHeight(image_url) {
                        //alert('in computeHeight with ',image_url)
                        var D = $q.defer();
                        // calculate appropriate height for width = 1070
                        //create an img element and use it to 

                        var img = new Image();
                        img.onload = function () {
                            var width = img.naturalWidth,
                                height = img.naturalHeight,
                                targetHeight = Math.round((targetWidth / width) * height)
                            console.log('Target height - ', targetHeight, ' targetWidth-', targetWidth)
                            //alert('updated values - '+targetHeight+' original-' +width+' '+height)
                            D.resolve(targetHeight)

                        };
                        img.src = image_url;
                        return D.promise;
                    }


                    function uploadToCloudinary(base64Img) {
                        var D = $q.defer();

                        cloudinary.upload(base64Img, {})
                            // This returns a promise that can be used for result handling
                            .then(function (resp) {
                                console.log('resp is ', resp.data.secure_url)
                                ////alert('image uploaded'+JSON.stringify(resp.data.secure_url))
                                var new_image_object = {
                                    url: resp.data.secure_url,
                                    data: resp.data
                                }
                                D.resolve(resp.data.secure_url)
                            })
                            .catch(function (err) {
                                console.log('err occured ' + JSON.stringify(err))
                                //alert('err in cloudinary.upload '+JSON.stringify(err))
                                D.reject(err)
                            })
                        return D.promise;
                    }


                }]
        });
    })

}
