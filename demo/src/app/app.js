//import angular from 'angular';

var angular = require('angular')
import '../style/app.css';

let app = () => {
    return {
        template: require('./app.html'),
        controller: 'AppCtrl',
        controllerAs: 'app'
    }
};

class AppCtrl {
    constructor() {
        this.url = 'https://github.com/preboot/angular-webpack';
    }
}


const MODULE_NAME = 'app';

require('../../../../schema-form')
//require('../modules')()

require('./ng-prettyjson.min')
require('./ng-prettyjson.min.css')
require('jsonformatter')

//alert('creating app module')
var ngModule = angular.module(MODULE_NAME, ['schemaForm', 'ngPrettyJson', 'jsonFormatter'])
    .directive('app', app)
    .controller('AppCtrl', AppCtrl);

require('./demo/schema.form.demo.component')(ngModule)
require('./demo/demo.service')(ngModule)
//require('../use-cases/route/usecases.route.js')(ngModule)

export default MODULE_NAME;
