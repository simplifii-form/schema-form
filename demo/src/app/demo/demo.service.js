module.exports = function(ngModule) {
    ngModule.service('demoService', ['$rootScope', '$q', '$timeout', function($rootScope, $q, $timeout) {
        var service = this

        service.demoCall = function(context) {
            console.log('received context in demoCall ', context)
            var D = $q.defer()

            $timeout(function() {
                D.resolve()
            }, 3000)

            return D.promise
        }
    }]);
}
