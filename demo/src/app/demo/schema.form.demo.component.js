module.exports = function (ngModule) {
    ngModule.component('schemaFormDemo', {
        template: require('./usecases.html'),
        controllerAs: 'demo',
        controller: ['$rootScope', function ($rootScope) {
            var demo = this

            //demo.schema = require('./demo.schema.json')
            demo.usecases = {
                textInput: require('./use-cases/1.text.inputs.json'),
                numberInput: require('./use-cases/2.number.inputs.json'),
                booleanInput: require('./use-cases/3.boolean.inputs.json'),
                singleSelect: require('./use-cases/4.single.selection.json'),
                multiSelect: require('./use-cases/5.multiple.selection.json'),
                remoteEnumerations: require('./use-cases/10.remote.enumerations.json'),
                dateTime: require('./use-cases/6.time.inputs.json'),
                submitForm: require('./use-cases/7.submit.form.json'),
                redirectForm: require('./use-cases/8.redirect.form.json'),
                populateForm: require('./use-cases/9.details.input.json'),
                multiFieldInput: require('./use-cases/11.multi.text.input.json'),
                multiFormInput: require('./use-cases/12.multi.form.input.json'),
                remember: require('./use-cases/13.remember.json')
            }
        }]
    })
}
