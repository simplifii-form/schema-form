module.exports = function(ngModule) {
    ngModule.component('sampleComponent', {
        template: require('./sample.component.html'),
        controllerAs: 'sample',
        require: {
            parent: '^^sample'
        },
        bindings: {

        },
        controller: ['$rootScope', function($rootScope) {

        }]
    })
}
