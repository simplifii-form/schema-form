module.exports = function(ngModule) {

    ngModule.config((['$stateProvider', function($stateProvider) {

        $stateProvider
            .state('state.name', {
                url: '/route',
                views: {
                    viewname: {
                        template: require('./templatepath.html'),
                        controller: 'sampleController'
                    }
                }

            })
    }]))
}
