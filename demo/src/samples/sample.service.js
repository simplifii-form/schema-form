module.exports = function(ngModule) {
    ngModule.service('sampleService', ['$rootScope', function($rootScope) {
        var service = this

        service.myFunc = function(x) {
            return x.toString(16);
        }
    }]);
}
