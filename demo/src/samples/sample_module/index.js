var loopback = require('loopback')
var glue = require("../glue")

module.exports = function(app, options) {
    console.log('Adding Pricing Module with options')

    var glueConfig = require('./glue-config.json')
    glue.attach(app, glueConfig, options, __dirname)

}
