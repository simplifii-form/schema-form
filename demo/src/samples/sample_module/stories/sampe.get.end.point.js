module.exports = function(app, options) {
    /*
      require and respond with the form corresponding to this SampleModel
    */

    var SampleModel = app.models.SampleModel

    SampleModel.computingMethod = function(param1, callback) {
        console.log('Fetching form for ', param1)


        callback(null, response)
    }

    SampleModel.remoteMethod('computingMethod', {
        accepts: {
            arg: 'param1',
            type: 'string'
        },
        returns: {
            arg: 'fields',
            type: 'array'
        },
        http: {
            path: '/computing_method',
            verb: 'get'
        }
    })


}
