module.exports = function(app, options) {

    var SampleModel = app.models.SampleModel

    SampleModel.computingMethod = function(param1, callback) {

        callback(null, response)

    }


    SampleModel.remoteMethod('computingMethod', {
        accepts: [{
            arg: 'param1',
            type: 'string'
        }],
        returns: {
            arg: 'updated',
            type: 'object'
        },
        http: {
            path: '/recompile',
            verb: 'post'
        }
    })

}
