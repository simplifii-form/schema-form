module.exports = function(app, options) {

    var SampleModel = app.models.SampleModel

    SampleModel.remoteFunction = function(arg1, callback) {
        console.log('request for remoteFunction ', arg1)
        callback(null, result)
    }

    SampleModel.remoteMethod('remoteFunction', {
        accepts: [{
            arg: 'arg1',
            type: 'string'
        }],
        returns: {
            arg: 'key',
            type: 'array'
        },
        http: {
            path: '/stub',
            verb: 'post'
        }
    })
}
