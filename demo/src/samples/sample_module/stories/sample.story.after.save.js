module.exports = function(app, options) {
    var SampleModel = app.models.SampleModel

    SampleModel.observe('after save', function(ctx, next) {

        var instance = ctx.data || ctx.currentInstance || ctx.instance

        if (instance) {
            //write logic here
        }

        next()
    })
}
