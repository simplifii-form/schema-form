module.exports = function(app, options) {
    var SampleModel = app.models.SampleModel

    SampleModel.observe('before save', function(ctx, next) {

        var instance = ctx.instance || ctx.data || ctx.currentInstance

        if (instance) {
            //write logic here
        }

        next()
    })
}
