// // var ngFormly = require('angular-formly')
// // // var formlyBootstrap = require('angular-formly-templates-bootstrap')
// // var formlyMaterial = require('angular-formly-material')
// // var toggleSwitch = require('angular-ui-switch')
// // var dateTimePicker = require('bootstrap-ui-datetime-picker')
// // var uiBootstrap = require('angular-ui-bootstrap')
// // var angularSlider = require('angularjs-slider')
// // var checklistModel = require('checklist-model')
// // var ngMessages = require('angular-messages')
// // require('./lib')
// // //var mutliselectDropdown = require('./lib/isteven-angular-multiselect/isteven-multi-select')

// // //var multi = require('angular-multiple-select')
// // //require('mustache')

// // angular.moment = require('moment')
// // angular.lodash = require('lodash')
// // var schemaFormModule = angular.module('schemaForm', [
// //     ngFormly,
// //     'ngMessages',
// //     'formlyMaterial',
// //     'uiSwitch',
// //     'ui.bootstrap',
// //     'ui.bootstrap.datetimepicker',
// //     'rzModule',
// //     'checklist-model',
// //     'isteven-multi-select'
// // ])

// // //disable api check.
// // var apiCheck = require('api-check');
// // apiCheck.globalConfig.disabled = true; //To see any errors, enable this

// // require('./components/schema.form.component')(schemaFormModule)
// // require('./services')(schemaFormModule)
// // require('./constants/default.interface.constant')(schemaFormModule)
// // require('./controls')(schemaFormModule)


// // require('font-awesome-webpack')
// // require('./styles/json-formatter.css')


// module.exports = schemaFormModule

module.exports = function(cloudinaryConfig){

var ngFormly = require('angular-formly')
var formlyBootstrap = require('angular-formly-templates-bootstrap')
//var formlyMaterial = require('angular-formly-material')
var toggleSwitch = require('angular-ui-switch')
var dateTimePicker = require('bootstrap-ui-datetime-picker')
var uiBootstrap = require('angular-ui-bootstrap')
var angularSlider = require('angularjs-slider')
var checklistModel = require('checklist-model')
var ngMessages = require('angular-messages')
require('./lib')
require('angular-file-upload')
//var mutliselectDropdown = require('./lib/isteven-angular-multiselect/isteven-multi-select')

//var multi = require('angular-multiple-select')
//require('mustache')

angular.moment = require('moment')
angular.lodash = require('lodash')
var schemaFormModule = angular.module('schemaForm', [
    ngFormly,
    'ngMessages',
    'formlyBootstrap',
    'uiSwitch',
    'ui.bootstrap',
    'ui.bootstrap.datetimepicker',
    'rzModule',
    'checklist-model',
    'isteven-multi-select',
    'angularFileUpload',
    'angular-cloudinary'
])

if(cloudinaryConfig){
	schemaFormModule.config(function (cloudinaryProvider) {
        cloudinaryProvider.config(cloudinaryConfig);
    })
}
console.log('ngFormly is ', ngFormly)
//disable api check.
var apiCheck = require('api-check');
apiCheck.globalConfig.disabled = true; //To see any errors, enable this

require('./components/schema.form.component')(schemaFormModule)
require('./services')(schemaFormModule)
require('./constants/default.interface.constant')(schemaFormModule)
require('./controls')(schemaFormModule)


require('font-awesome-webpack')
require('./styles/json-formatter.css')
}