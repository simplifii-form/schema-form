module.exports = function (ngModule) {
    ngModule.factory('Camera', function ($q) {

        return {
            getPicture: function (options) {
                var q = $q.defer();

                if (navigator.camera) {
                    navigator.camera.getPicture(function (result) {
                        q.resolve(result);
                    }, function (err) {
                        q.reject(err);
                    }, options);
                } else {
                    q.reject('Camera not found on navigator')
                }


                return q.promise;
            }
        }
    })
}

