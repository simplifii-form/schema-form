module.exports = function (ngModule) {
    require('./schema.to.formly.service')(ngModule)
    require('./populate.enumerations.factory')(ngModule)

    require('./schema.actions.service')(ngModule)
    require('./schema.actions.collect.service')(ngModule)
    require('./schema.actions.submit.service')(ngModule)
    require('./schema.actions.final.service')(ngModule)

    require('./camera.factory')(ngModule)
}
