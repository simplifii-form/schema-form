module.exports = function(ngModule) {
    ngModule.factory('populateEnumerations', function() {



        return function(enumerations, model) {
            var options = []
            var populate = {
                options: options
            }

            if (enumerations && enumerations.source == 'values') {
                //options can be picked from the values
                enumerations.values.map(function(value) {
                    if (angular.isObject(value)) {
                        //set label and value using labelKey and valueKey
                        options.push({
                            label: value[enumerations.labelKey],
                            value: (enumerations.valueKey) ? (value[enumerations.valueKey]) : (angular.copy(value))
                        })
                    } else {
                        //set label and value as the value string/number
                        options.push({
                            label: value,
                            value: value
                        })
                    }
                })
            }


            return populate
        }

    })
}
