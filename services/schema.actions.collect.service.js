module.exports = function(ngModule) {
    ngModule.service('SchemaActionCollect', ['$rootScope', '$q', function($rootScope, $q) {
        var service = this

        service.collect = function(model, context, collect) {
            //TODO validate before collecting data
            console.log('collect called ', model, context, collect)
            switch (collect.source) {
                case 'form':
                    if (collect.fields) {

                        context.data = angular.lodash.pick(model, collect.fields)

                    } else {
                        context.data = angular.copy(model)
                    }

                    console.log('collected data on context ', context)
                    return $q.resolve()
                default:
                    return $q.reject()
            }

        }
    }])
}
