module.exports = function(ngModule) {
    ngModule.service('SchemaActionFinal', ['$rootScope', '$q', '$interpolate', '$injector', function($rootScope, $q, $interpolate, $injector) {
        var service = this

        service.final = function(model, context, final) {
            switch (final.operation) {
                case 'redirect':
                    var to = final.url || final.path
                    var exp = $interpolate(to)
                    var dst = exp(context)
                    if (final.url) {
                        $location.url(dst)
                    } else {
                        $location.path(dst)
                    }
                    return $q.resolve()
                case 'serviceCall':
                    return $injector.get(final.service)[final.call](context)

                default:
                    return $q.reject('Final Operation is not valid')
            }
        }
    }]);
}
