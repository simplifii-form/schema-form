module.exports = function(ngModule) {
    ngModule.service('SchemaActions', ['$rootScope', '$q', 'SchemaActionCollect', 'SchemaActionSubmit', 'SchemaActionFinal',
        function($rootScope, $q, SchemaActionCollect, SchemaActionSubmit, SchemaActionFinal) {
            var service = this

            service.action = function(model, context, action) {
                console.log('action called with ', model, context, action)
                if (action.collect) {
                    context.state = 'processing'
                    return SchemaActionCollect.collect(model, context, action.collect)
                        .then(function() {
                            return callSubmitIfNeeded(model, context, action)
                        })
                        .then(function() {
                            return SchemaActionFinal.final(model, context, action.final)
                        })
                        .then(function() {
                            context.state = 'idle'
                            return $q.resolve()
                        })
                        .catch(function(err) {
                            context.state = 'idle'
                            console.log('Unable to complete action ', err)
                        })
                } else {
                    console.log('There is no collect option defined on the action')
                }
            }

            function callSubmitIfNeeded(model, context, action) {
                if (action.submit) {
                    return SchemaActionSubmit.submit(model, context, action.submit)
                        .then(function(response) {
                            console.log('response of submit ', response)
                            context.response = response
                            //check success criteria
                            if (angular.lodash.isMatch(response, action.submit.success)) {
                                return $q.resolve()
                            } else {
                                return $q.reject('Success criteria does not match')
                            }
                        })
                } else {
                    return $q.resolve()
                }
            }
        }
    ])
}
