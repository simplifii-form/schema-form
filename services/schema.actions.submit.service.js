module.exports = function(ngModule) {
    ngModule.service('SchemaActionSubmit', ['$rootScope', '$interpolate', '$http', function($rootScope, $interpolate, $http) {
        var service = this

        service.submit = function(model, context, submit) {
            console.log('submit called ', model, context, submit)
            if (submit.destination == 'url') {
                var exp = $interpolate(submit.url)
                var url = exp(context)

                console.log('sending data ', context.data)
                return $http[submit.method](url, context.data)
            } else {
                return $q.reject()
            }


        }
    }]);
}
