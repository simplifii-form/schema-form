module.exports = function (ngModule) {
    ngModule.service('SchemaToFormly', ['defaultFormlyJson', 'populateEnumerations',
        function (defaultFormlyJson, populateEnumerations) {
            var service = this

            service.toFormly = schemaToFormly


            function schemaToFormly(schema, model) {

                var formly_fields = schema.properties.map(function (property) {
                    var formly_field = {
                        key: property.key,
                        dataType: property.type,
                        templateOptions: {
                            label: property.title,
                            labelProp: 'label',
                            valueProp: 'value'
                        }
                    }
                    //console.log('property ', property)
                    //console.log('formly_field templateOptions ', formly_field.templateOptions, 'label ', property.title)

                    // //attach control
                    var formly_type = null
                    if (property.interface && property.interface.control) {
                        formly_field.type = property.interface.control
                    } else {
                        if (property.type == 'array' && (property.field_schema || property.form_schema)) {
                            formly_field.type = 'array'
                        } else {
                            var formly_json = defaultFormlyJson[property.type]
                            formly_field = angular.lodash.merge({}, formly_json, formly_field)
                        }
                    }


                    // //attach nested fields
                    if (property.schema) {
                        formly_field.fieldGroup = schemaToFormly(property.schema)
                    }

                    formly_field.interface = property.interface
                    formly_field.format = property.format
                    formly_field.validations = property.validations
                    formly_field.enumerations = property.enumerations
                    formly_field.action = property.action
                    formly_field.detail_schema = property.detail_schema
                    formly_field.form_schema = property.form_schema
                    formly_field.field_schema = property.field_schema
                    formly_field.populate = populateEnumerations(formly_field.enumerations, model)

                    //set options on the templateOptions
                    formly_field.templateOptions.options = formly_field.populate.options

                    // console.log('created formly field 2', angular.copy(formly_field))
                    return formly_field
                })


                return formly_fields

            }
        }
    ])
}
